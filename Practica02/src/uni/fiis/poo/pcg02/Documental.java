package uni.fiis.poo.pcg02;

public class Documental extends MaterialFilmico {
    private String capitulo;

    public Documental(String nombre, String descripcion, String duracion, String capitulo) {
        super(nombre, descripcion, "Documental", duracion);
        this.capitulo = capitulo;
    }



    public void mostrarDatos() {
        System.out.println("Documental: ");
        super.mostrarDatos();
        System.out.println(  "capitulo='" + capitulo);
    }
}
