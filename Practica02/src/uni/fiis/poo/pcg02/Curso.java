package uni.fiis.poo.pcg02;

import java.util.ArrayList;

public class Curso {
    private String nombre;
    private String ciclo;
    private String Universidad;
    private ArrayList<MaterialUniversitario> prodUnivs;

    public Curso(String nombre, String ciclo, String universidad, ArrayList<MaterialUniversitario> prodUnivs) {
        this.nombre = nombre;
        this.ciclo = ciclo;
        Universidad = universidad;
        this.prodUnivs = prodUnivs;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCiclo() {
        return ciclo;
    }

    public String getUniversidad() {
        return Universidad;
    }

    public ArrayList<MaterialUniversitario> getProdUnivs() {
        return prodUnivs;
    }

    @Override
    public String toString() {
        return "Curso{" +
                "nombre='" + nombre + '\'' +
                ", ciclo='" + ciclo + '\'' +
                ", Universidad='" + Universidad + '\'' +
                ", prodUnivs=" + prodUnivs +
                '}';
    }
}
