package uni.fiis.poo.pcg02;

public class Libro extends MaterialUniversitario {
    private String codISBN;
    private String autor;
    private int paginas;
    private int anio;

    public Libro(String nombre, String descripcion, String codISBN, String autor, int paginas, int anio) {
        super(nombre, descripcion);
        this.codISBN = codISBN;
        this.autor = autor;
        this.paginas = paginas;
        this.anio = anio;
    }

    public void mostrarDatos() {
        System.out.println("Libro{" +
                "codISBN='" + codISBN + '\'' +
                ", autor='" + autor + '\'' +
                ", paginas=" + paginas +
                ", anio=" + anio +
                '}');
    }
}
