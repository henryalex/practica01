package uni.fiis.poo.pcg02;

public abstract class MaterialUniversitario {
    protected String nombre;
    protected String descripcion;


    public MaterialUniversitario(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public abstract void mostrarDatos();

    /*
    public void mostrarDatos() {
        System.out.println("nombre ='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}');
    }
    */

}
