package uni.fiis.poo.pcg02;

public class MaterialFilmico extends MaterialUniversitario {
    protected String genero;
    protected String duracion;

    public MaterialFilmico(String nombre, String descripcion, String genero, String duracion) {
        super(nombre, descripcion);
        this.genero = genero;
        this.duracion = duracion;
    }


    public void mostrarDatos() {

    }
}
