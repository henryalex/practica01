package uni.fiis.poo.pc1;


public class Vuelo {
    private int numero;
    private Aerolinea aerolinea;
    private Aeronave aeronave;
    private String origen;
    private String destino;
    private String fechaSalida;
    private String duracionVuelo;
    private int asientosTotales; //Numero total de asientos
    private String listaAsiento[]; //Arreglo con la lista de asientos
    private String asientosReservados[]; //Arreglo con lo asientos reservados

    public Vuelo(int numero, Aerolinea aerolinea, Aeronave aeronave, String origen, String destino, String fechaSalida, String duracionVuelo, int asientosTotales) {
        this.numero = numero;
        this.aerolinea = aerolinea;
        this.aeronave = aeronave;
        this.origen = origen;
        this.destino = destino;
        this.fechaSalida = fechaSalida;
        this.duracionVuelo = duracionVuelo;
        this.asientosTotales = asientosTotales;
        this.listaAsiento = new String[asientosTotales];
    }

    public void setAsientosReservados(String[] asientosReservados) {
        this.asientosReservados = asientosReservados;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    //Llenado del arreglo de asientos con su respectivo numero
    public void llenarAsientos(){
        for(int i=0;i<asientosTotales;i++){
            listaAsiento[i]=String.valueOf(i+1);
        }
    }

    public void mostrarAsientos(){
            for(int i=0;i<asientosTotales;i++){
                System.out.print("["+listaAsiento[i]+"]");
                if(i==(asientosTotales/4)||i==asientosTotales/2||i==((asientosTotales*3)/4)){
                    System.out.println("");
                }
            }
    }

    public boolean asignarAsiento(String nasiento){
        boolean aux=true;
        if(listaAsiento[Integer.parseInt(nasiento)-1]!="ND"){
            listaAsiento[Integer.parseInt(nasiento)-1]="ND";
        } else{
            System.out.println("¡El asiento esta ocupado.!");
            aux=false;
        }
        return aux;

    }

    public void mostrarDatos(){
        System.out.println("\nNumero de Vuelo: "+numero+
                "\nCiudad de Origen: "+origen+" Ciudad de Destino: "+destino+
                "\nFecha y Hora de Salida: "+fechaSalida+" Duracion Estimado: " +duracionVuelo+
                "\nAerolinea: "+aerolinea.getNombre()+" Direccion: "+aerolinea.getDireccion()+"Email: " +aerolinea.getCorreo()+
                "\nPágina web: "+aerolinea.getPaginaWeb()+
                "\nAeronave: "+aeronave.getFabricante()+" "+aeronave.getModelo()+" Capacidad: "+aeronave.getCapacidad()+" Fecha de Compra: "+aeronave.getFechaCompra());
        System.out.println("");

    }

    public void mostarAsientosReservados(){
        for(int i=0;i<asientosReservados.length;i++){
            System.out.print("["+asientosReservados[i]+"] ");
        }
    }



}
