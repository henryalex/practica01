package uni.fiis.poo.pc1;


import java.util.ArrayList;

public class Reserva {
    private int codigo;
    private Cliente cliente;
    private ArrayList<Vuelo> vuelos;

    public Reserva(int codigo, Cliente cliente, ArrayList<Vuelo> vuelos) {
        this.codigo = codigo;
        this.cliente = cliente;
        this.vuelos = vuelos;
    }

    public void mostrarReserva() {
        System.out.println("\nCodigo de Reserva: " + codigo + "\nDatos del pasajero" + "\nNombre y Apellidos: " + cliente.getNombre() + " " + cliente.getApellido() +
                "\nDNI: " + cliente.getDni()+"\nCorreo Electronico: "+cliente.getCorreo()+"\nTelefono: "+cliente.getTelefono());
    }

    public void mostrarVuelosReservados(){
        for(Vuelo vu: vuelos){
            System.out.println("\nVuelo Numero: "+(vuelos.indexOf(vu)+1)+"\nOrigen: "+vu.getOrigen()+"\nDestino: "+vu.getDestino()+
            "\nFecha y hora de salida: "+vu.getFechaSalida()+
            "\nAsientos Reservados: ");
            vu.mostarAsientosReservados();
        }

    }
}
