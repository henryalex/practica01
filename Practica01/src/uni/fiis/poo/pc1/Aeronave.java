package uni.fiis.poo.pc1;


public class Aeronave {
    private String fabricante;
    private String modelo;
    private int capacidad;
    private String fechaCompra;

    public Aeronave(String fabricante, String modelo, int capacidad, String fechaCompra) {
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.capacidad = capacidad;
        this.fechaCompra = fechaCompra;
    }

    public String getFabricante() {
        return fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }


}
