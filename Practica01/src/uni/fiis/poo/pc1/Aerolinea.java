package uni.fiis.poo.pc1;

import java.util.ArrayList;

public class Aerolinea {
    private String nombre;
    private String paginaWeb;
    private String direccion;
    private String correo;
    private ArrayList<Aeronave> aeronaves;


    public Aerolinea(String nombre, String paginaWeb, String direccion, String correo, ArrayList<Aeronave> aeronaves) {
        this.nombre = nombre;
        this.paginaWeb = paginaWeb;
        this.direccion = direccion;
        this.correo = correo;
        this.aeronaves = aeronaves;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCorreo() {
        return correo;
    }


}
