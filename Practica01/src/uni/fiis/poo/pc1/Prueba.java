package uni.fiis.poo.pc1;

import java.util.ArrayList;
import java.util.Scanner;

public class Prueba {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int opcion, cont = 0;
        char op;
        String asiento;


        Aeronave aeronave1 = new Aeronave("Boeing", "767", 50, "09/03/2001");
        Aeronave aeronave2 = new Aeronave("Boeing", "717", 35, "12/07/2005");
        Aeronave aeronave3 = new Aeronave("Airbus", "A318", 50, "29/07/2009");
        Aeronave aeronave4 = new Aeronave("Airbus", "A321", 50, "12/08/2010");
        Aeronave aeronave5 = new Aeronave("Bombardier", "CRJ-1000", 40, "24/05/2013");
        Aeronave aeronave6 = new Aeronave("Bombardier", "CRJ-200ER", 45, "11/06/2011");

        ArrayList<Aeronave> aeronaves1 = new ArrayList<>();
        aeronaves1.add(aeronave1);
        aeronaves1.add(aeronave2);
        aeronaves1.add(aeronave3);

        ArrayList<Aeronave> aeronaves2 = new ArrayList<>();
        aeronaves2.add(aeronave4);
        aeronaves2.add(aeronave5);
        aeronaves2.add(aeronave6);

        Aerolinea aerolinea1 = new Aerolinea("LATAM","www.latam.com" ,"Av. Garcilaso de la Vega 1337", "informes@latam.com", aeronaves1);
        Aerolinea aerolinea2 = new Aerolinea("Avianca", "www.avianca.com","Av. José Pardo 831", "informes@avianca.com", aeronaves2);

        Vuelo vuelo1 = new Vuelo(1, aerolinea1, aeronave1, "Lima, Peru", "Santiago, Chile", "08/07/2020 18:30", "3 horas", aeronave1.getCapacidad());
        vuelo1.llenarAsientos();
        vuelo1.asignarAsiento("14");
        vuelo1.asignarAsiento("15");
        vuelo1.asignarAsiento("32");
        Vuelo vuelo2 = new Vuelo(2, aerolinea2, aeronave4, "Lima, Peru", "Buenos Aires, Argentina", "16/07/2020 20:30", "4 horas", aeronave4.getCapacidad());
        vuelo2.llenarAsientos();
        Vuelo vuelo3 = new Vuelo(3, aerolinea1, aeronave2, "Lima, Peru", "Bogota, Colombia", "13/07/2020 23:30", "2 horas, 30 min", aeronave2.getCapacidad());
        vuelo3.llenarAsientos();
        Vuelo vuelo4 = new Vuelo(4, aerolinea2, aeronave5, "Lima, Peru", "Rio de Janeiro, Brasil", "17/07/2020 14:30", "4 horas, 30 min", aeronave5.getCapacidad());
        vuelo4.llenarAsientos();

        ArrayList<Vuelo> vuelosDisponibles = new ArrayList<>();
        vuelosDisponibles.add(vuelo1);
        vuelosDisponibles.add(vuelo2);
        vuelosDisponibles.add(vuelo3);
        vuelosDisponibles.add(vuelo4);

        System.out.println("************************++++++++++++++++++++++*");
        System.out.println("Binvenido al sistema de reserva de VuelaAhora");
        System.out.println("************************++++++++++++++++++++++*");

        System.out.println("Registre sus datos peronsales");

        String dni, nomb, apell, correo, tel, alergia;

        System.out.print("Ingrese el DNI: ");
        dni = sc.nextLine();
        System.out.print("Ingrese el Nombre: ");
        nomb = sc.nextLine();
        System.out.print("Ingrese el Apellido: ");
        apell = sc.nextLine();
        System.out.print("Ingrese el Correo: ");
        correo = sc.nextLine();
        System.out.print("Ingrese el Telefono: ");
        tel = sc.nextLine();
        System.out.print("Indique si tiene alguna alergia: ");
        alergia = sc.nextLine();

        Cliente cliente = new Cliente(dni, nomb, apell, correo, tel, alergia);

        ArrayList<Vuelo> vuelosReservados = new ArrayList<>();

        do {
            System.out.println("\nElija el numero de destino: ");
            for (Vuelo vu : vuelosDisponibles) {
                System.out.println((vuelosDisponibles.indexOf(vu) + 1) + ".- " + vu.getDestino());
            }
            opcion = sc.nextInt();

            System.out.println("Esta es la informacion del vuelo para ese destino: ");
            vuelosDisponibles.get((opcion - 1)).mostrarDatos();
            System.out.print("¿Desea generar una reserva de este vuelo? (s/n): ");

            do {
                op = sc.next().charAt(0);
                switch (op) {
                    case 's':
                    case 'S':

                        System.out.print("¿Cuantos asientos desea reservar?: ");
                        int n = sc.nextInt();
                        sc.nextLine();
                        String[] asientos = new String[n];
                        for (int i = 0; i < n; i++) {
                            System.out.println("Lista de asientos");
                            vuelosDisponibles.get((opcion - 1)).mostrarAsientos();
                            System.out.println("");
                            do {
                                System.out.print("Ingrese el numero del asiento N°"+(i+1)+": ");
                                asiento = sc.nextLine();
                            } while (!vuelosDisponibles.get((opcion - 1)).asignarAsiento(asiento));
                            asientos[i] = asiento;
                        }

                        vuelosReservados.add(vuelosDisponibles.get((opcion - 1)));
                        vuelosReservados.get(cont).setAsientosReservados(asientos);
                        cont++;
                        vuelosDisponibles.remove(opcion - 1);

                        System.out.println("Reserva añadido con exito");

                        break;
                    case 'n':
                    case 'N':
                        System.out.println("No se realizo la reserva. Gracias por su visita.");
                        break;
                    default:
                        System.out.println("Opcion invalida. Ingrese otra opcion");
                        break;
                }
            } while (op != 's' && op != 'S' && op != 'n' && op != 'N');

            if (vuelosDisponibles.size() != 0) {
                System.out.println("¿Desea reservar otro vuelo?(s/n)");
                op = sc.next().charAt(0);
            }
        } while ((op == 's' || op == 'S') && vuelosDisponibles.size() != 0);

        Reserva reserva = new Reserva(1, cliente, vuelosReservados);

        //Mostrando las reservas guardadas
        System.out.println("\nSu Reserva es la siguiente");
        reserva.mostrarReserva();
        reserva.mostrarVuelosReservados();
    }

}
