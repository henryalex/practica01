package uni.fiis.poo.pc1;

public class Cliente {
    private String dni;
    private String nombre;
    private String apellido;
    private String correo;
    private String telefono;
    private String alergias;

    public Cliente(String dni, String nombre, String apellido, String correo, String telefono, String alergias) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.telefono = telefono;
        this.alergias = alergias;
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTelefono() {
        return telefono;
    }

}
